import playerC as pla
import money as mon
import card as car

phase = 0
turn = 0

Aid = [] #List of all the foreign aid
Deck = [] #List of all the avalible cards

#Adding all the cards to the deck
Deck.append(car.Card("Monarchists","During an electino or roll call vote, or during Phase 1: Drawing Political Cards.",3,False))
Deck.append(car.Card("Farmers","During an election or roll call vote, or during Phase 1: Drawing Political Cards.",3,False))
Deck.append(car.Card("Bankers","During an election or roll call vote, or during Phase 1: Drawing Political Cards.",7,False))
Deck.append(car.Card("Labor Union","During an election or roll call vote, or during Phase 1: Drawing Political Cards.",5,False))
Deck.append(car.Card("Radicals","During an election or roll call vote, or during Phase 1: Drawing Political Cards.",3,False))
Deck.append(car.Card("Students","During an election or roll call vote, or during Phase 1: Drawing Political Cards.",3,False))
Deck.append(car.Card("The Church","During an election or roll call vote.",10,False))
Deck.append(car.Card("Conservatives","During an election or roll call vote, or during Phase 1: Drawing Political Cards.",8,False))
Deck.append(car.Card("Socialists","During an election or roll call vote, or during Phase 1: Drawing Political Cards.",5,False)) #This is a special card its 5 votes normally but 10 votes if you also have the Labor Union card face-up infront of you we will need to figure something out to do this
Deck.append(car.Card("Christian Democrats","During an election or roll call vote, or during Phase 1: Drawing Political Cards.",5,False)) #This is a special card its 5 votes normally but 10 votes if you also have the Church card face-up in front of you we will need to figure something out to do this
Deck.append(car.Card("University Professors","During an election or roll call vote, or during Phase 1: Drawing Political Cards.",3,False)) #This is a special card its 3 votes normally but 6 votes if you also have the Students card face-up in front of you we will need to figure something out to do this
#Adding temporary voting cards
Deck.append(car.Card("Retired Generals","During an election or roll call vote.",3,True))
Deck.append(car.Card("Farmers","During an election or roll call vote.",2,True))
Deck.append(car.Card("Political Refugees","During an election or roll call vote",2,True))
Deck.append(car.Card("The Black Market","During an election or roll call vote.",3,True))
Deck.append(car.Card("Graveyard Voters","During an election or roll call vote",3,True))
Deck.append(car.Card("Anarchists","During an election or roll call vote.",2,True))
Deck.append(car.Card("Multiple Votes","During an elecitno or roll call vote",2,True))
Deck.append(car.Card("Communists","During an election or roll call vote.",2,True))
Deck.append(car.Card("The Middle Class","During an election or roll call vote.",3,True))
#Adding Secret Party Donations
Deck.append(car.Card("Secret Party Donation 1","At any time during your turn",0,False))
Deck.append(car.Card("Secret Party Donation 2","At any time during your turn",0,False))
Deck.append(car.Card("Secret Party Donation 3","At any time during your turn",0,False))
Deck.append(car.Card("Secret Party Donation 4","At any time during your turn",0,False))
#Adding Assassins
Deck.append(car.Card)

#Adding all the money to a list
for z in range(51):
    Aid.append(mon.Money(1))
for c in range(30):
    Aid.append(mon.Money(2))
for v in range(15):
    Aid.append(mon.Money(3))



print("How many players are playing?")
numOfPlayers = input()
for x in numOfPlayers:
    if(x == 0):
        Player0 = pla.Player
    if(x == 1):
        Player1 = pla.Player
    if(x == 2):
        Player2 = pla.Player
    if(x == 3):
        Player3 = pla.Player
    if(x == 4):
        Player4 = pla.Player
    if(x == 5):
        Player5 = pla.Player
    if(x == 6 ):
        Player6 = pla.Player
