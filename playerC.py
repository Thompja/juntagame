# A class for every player that lays out everything that we need to keep track of for them.

class Player:
    def __init__(self,cash = 0,id = 0,swiss = [], role = "citizen"):
        self.cash = cash
        self.id = id
        self.swiss = swiss
        self.role = role